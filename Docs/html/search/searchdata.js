var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuvw",
  1: "abdjnpr",
  2: "abdjpr",
  3: "abdefgijlmnoprstuw",
  4: "abcdefghijmnoprstuvw",
  5: "w",
  6: "acg"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

