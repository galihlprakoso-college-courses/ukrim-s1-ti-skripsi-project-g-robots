var annotated_dup =
[
    [ "AIController", "class_a_i_controller.html", "class_a_i_controller" ],
    [ "AIControllerFight", "class_a_i_controller_fight.html", "class_a_i_controller_fight" ],
    [ "AIHealth", "class_a_i_health.html", "class_a_i_health" ],
    [ "AIHealthFight", "class_a_i_health_fight.html", "class_a_i_health_fight" ],
    [ "ButtonOnClick", "class_button_on_click.html", "class_button_on_click" ],
    [ "DestroyProjectile", "class_destroy_projectile.html", "class_destroy_projectile" ],
    [ "DoxygenConfig", "class_doxygen_config.html", "class_doxygen_config" ],
    [ "DoxygenWindow", "class_doxygen_window.html", "class_doxygen_window" ],
    [ "DoxyRunner", "class_doxy_runner.html", "class_doxy_runner" ],
    [ "DoxyThreadSafeOutput", "class_doxy_thread_safe_output.html", "class_doxy_thread_safe_output" ],
    [ "JaringanSarafTiruan", "class_jaringan_saraf_tiruan.html", "class_jaringan_saraf_tiruan" ],
    [ "Neuron", "class_neuron.html", "class_neuron" ],
    [ "PlayerController", "class_player_controller.html", "class_player_controller" ],
    [ "PlayerHealth", "class_player_health.html", "class_player_health" ],
    [ "PlayerHealthFight", "class_player_health_fight.html", "class_player_health_fight" ],
    [ "projectTileController", "classproject_tile_controller.html", "classproject_tile_controller" ],
    [ "RocketHit", "class_rocket_hit.html", "class_rocket_hit" ],
    [ "RocketHitPlayer", "class_rocket_hit_player.html", "class_rocket_hit_player" ]
];