﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthFight : MonoBehaviour {

    public float fullHealth;

    float currentHealth;

    PlayerController playerController;

    public GameObject aiObject;

    public Slider healthSlider;

    private JaringanSarafTiruan jst;

	// Use this for initialization
	void Start () {
        currentHealth = fullHealth;
        playerController = GetComponent<PlayerController>();

        healthSlider.maxValue = fullHealth;
        healthSlider.value = currentHealth;
        
        jst = new JaringanSarafTiruan();
	}

    public void addDamage(float damage)
    {
        if (damage <= 0) return;
        currentHealth -= damage;

        healthSlider.value = currentHealth;

        if (currentHealth <= 0)
        {
            backToMenu();
        }
    }

    public void backToMenu()
    {
        //Play dead animation     
        Application.LoadLevel(0);
    }
}
