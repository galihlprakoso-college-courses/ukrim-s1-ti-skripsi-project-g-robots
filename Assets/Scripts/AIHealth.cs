﻿using UnityEngine;
using UnityEngine.UI;

public class AIHealth : MonoBehaviour {

    public float fullHealth;

    float currentHealth;

    AIController aiController;

    public Slider healtSlider;

    public Text loadingText;

    private JaringanSarafTiruan jst;

    // Use this for initialization
    void Start()
    {
        currentHealth = fullHealth;
        aiController = GetComponent<AIController>();

        healtSlider.maxValue = fullHealth;
        healtSlider.value = currentHealth;

        jst = new JaringanSarafTiruan();

        loadingText.gameObject.SetActive(false);
    }

    public void addDamage(float damage)
    {
        if (damage <= 0) return;
        currentHealth -= damage;

        healtSlider.value = currentHealth;        

        if (currentHealth <= 0)
        {
            loadingText.gameObject.SetActive(true);
            trainJST();
        }
    }

    public void trainJST()
    {        

        if (PlayerPrefs.GetInt("nInputs") == null)
        {
            jst.train(aiController.dataTraining,false);
        }
        else
        {
            jst.train(aiController.dataTraining, true);
        }

        Application.LoadLevel(0);
    }
}



