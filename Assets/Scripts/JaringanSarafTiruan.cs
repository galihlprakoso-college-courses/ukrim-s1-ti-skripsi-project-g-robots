﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class JaringanSarafTiruan:MonoBehaviour{

    public List<List<Neuron>> network;
    

    public JaringanSarafTiruan()
    {
        network = new List<List<Neuron>>();

        /*
        foreach(var layer in network)
        {
            foreach(var neuron in layer)
            {
                Debug.Log("==============================");
                Debug.Log("weights :");
                foreach(var w in neuron.weights)
                {
                    Debug.Log(w);
                }
                Debug.Log("==============================");
            }
        }*/
    }
    //Checklist [v]
    private void initializeNetwork(int nInputs,int nHidden,int nOutputs)
    {           
        List<Neuron> hiddenLayer = new List<Neuron>();

        //Inisialisasi hidden layer
        for (int i = 0; i < nHidden; i++)
        {
            Neuron neuron = new Neuron();

            for (int j = 0; j < nInputs + 1; j++)
            {
                System.Random rand = new System.Random();
                Double nilai = rand.NextDouble();
                //Debug.Log(nilai);
                float weightValue;
                try
                {
                    weightValue = float.Parse(nilai.ToString());
                }catch(Exception e)
                {
                    weightValue = float.Parse(nilai.ToString().Substring(0, nilai.ToString().Length-5));
                }
                //Debug.Log("Hidden Layer, Neuron ke " + i + " Weight ke "+j+", Value Weight :"+weightValue);
                //weightValue + (float.Parse(i.ToString()) / 10f)
                try
                {
                    neuron.weights.Add(weightValue + (float.Parse(i.ToString()) / 10f));
                }catch(Exception e)
                {
                    neuron.weights.Add(weightValue + (float.Parse(i.ToString().Substring(0, i.ToString().Length-5)) / 10f));
                }
            }

            hiddenLayer.Add(neuron);
        }

        network.Add(hiddenLayer);

        List<Neuron> outputLayer = new List<Neuron>();

        //Inisialisasi output layer
        for (int i = 0; i < nOutputs; i++)
        {
            Neuron neuron = new Neuron();

            for (int j = 0; j < nHidden + 1; j++)
            {
                System.Random rand = new System.Random();
                Double nilai = rand.NextDouble();
                //Debug.Log(nilai);
                float weightValue;
                try
                {
                    weightValue = float.Parse(nilai.ToString());
                }catch(Exception e)
                {
                    weightValue = float.Parse(nilai.ToString().Substring(0, nilai.ToString().Length-5));
                }
                //Debug.Log("Output Layer, Neuron ke " + i + " Weight ke " + j + ", Value Weight :" + weightValue);
                //weightValue + (float.Parse(i.ToString()) / 10f)
                try
                {
                    neuron.weights.Add(weightValue + (float.Parse(i.ToString()) / 10f));
                }catch(Exception e)
                {
                    neuron.weights.Add(weightValue + (float.Parse(i.ToString().Substring(0, i.ToString().Length-5)) / 10f));
                }
            }

            outputLayer.Add(neuron);
        }

        network.Add(outputLayer);        
    }

    //Checklist [VVV]
    private float activate(List<float> weights,List<float> inputs)
    {
        float activation = weights[weights.Count - 1];

        //Debug.Log("activation-before : " + activation);

        for (int i = 0; i < weights.Count-1; i++)
        {
            //Debug.Log("weights.Count : " + weights.Count);
            //Debug.Log("inputs.Count : " + inputs.Count);
            //Debug.Log("i : " + i);
            activation += weights[i] * inputs[i];
        }

        //Debug.Log("activation-after : " + activation);

        return activation;
    }

    //Checklist [VVV]
    private float transfer(float activation)
    {

        double converted;

        try
        {
            converted = Double.Parse(activation.ToString());
        }catch(Exception e)
        {
            converted = Double.Parse(activation.ToString().Substring(0, activation.ToString().Length-5));
        }

        float converted2;

        try
        {
            converted2 = float.Parse(Math.Exp(-converted).ToString());
        }catch(Exception e)
        {
            converted2 = float.Parse(Math.Exp(-converted).ToString().Substring(0, Math.Exp(-converted).ToString().Length-4));
        }

        try
        {
            return float.Parse((1f / (1f + converted2)).ToString());
        }catch(Exception e)
        {
            return float.Parse((1f / (1f + converted2)).ToString().Substring(0, (1f / (1f + converted2)).ToString().Length-5));
        }
    }

    //Checklist [VVV]
    private List<float> forwardPropagate(List<float> row)
    {
        List<float> inputs = row;

        foreach (var layer in network)
        {
            var newInputs = new List<float>();
            foreach (var neuron in layer)
            {
                var n = neuron;

                float activation = activate(neuron.weights, inputs);                
                n.output = transfer(activation);                
                newInputs.Add(neuron.output);
            }
            inputs = newInputs;
        }

        return inputs;
    }
    
    //Checklist [VVV]
    private float transferDerivative(float output)
    {        
        return output * (1f - output);
    }

    //Checklist [VVV]
    private void backwardPropagateError(List<float> expected)
    {
        for (int i = network.Count-1; i >= 0 ; i--)
        {
            var layer = network[i];

            var errors = new List<float>();

            if(i != network.Count - 1)
            {
                for (int j = 0; j < layer.Count; j++)
                {
                    var error = 0f;
                    foreach(var neuron in network[i + 1])
                    {
                        error += (neuron.weights[j] * neuron.delta);
                    }
                    errors.Add(error);
                }
            }
            else
            {
                for (int j = 0; j < layer.Count; j++)
                {
                    var neuron = layer[j];
                    var print = expected[j] - neuron.output;                    
                    errors.Add(expected[j] - neuron.output);
                }
            }

            for (int j = 0; j < layer.Count; j++)
            {
                var neuron = layer[j];
                neuron.delta = errors[j] * transferDerivative(neuron.output);
            }
        }              
    }

    //Checklist [VVV]
    private void updateWeights(List<float> row, float lRate)
    {
        for (int i = 0; i < network.Count; i++)
        {
            var inputs = row.GetRange(0,network.Count-1);          

            if (i != 0)
            {
                inputs = new List<float>();
                foreach (var neuron in network[i - 1])
                {
                    inputs.Add(neuron.output);
                }                
            }
            foreach(var neuron in network[i])
            {
                for (int j = 0; j < inputs.Count; j++)
                {
                    neuron.weights[j] += lRate * neuron.delta * inputs[j];                    
                }
                neuron.weights[neuron.weights.Count-1] += lRate * neuron.delta;                
            }
        }
    }

    //Checklist [VVV]
    private void trainNetwork(List<List<float>> train, float lRate, int epoch, float nOutputs)
    {    

        var eCount = 1;
        for (int i = 0; i < epoch; i++)
        {            
            var sumError = 0f;
            //Debug.Log("Epoch : " + eCount.ToString());
            foreach (var row in train)
            {
                if (row.Count != 0)
                {
                    var outputs = forwardPropagate(row);
                    var expected = new List<float>();
                    for (int j = 0; j < nOutputs; j++)
                    {
                        expected.Add(0f);
                    }

                    expected[int.Parse((row[row.Count - 1]).ToString())] = 1f;

                    var sum = 0f;

                    for (int j = 0; j < expected.Count; j++)
                    {
                        try
                        {
                            var num = Double.Parse((expected[j] - outputs[j]).ToString());
                            sum += float.Parse((Math.Pow(num, 2)).ToString());
                        }catch(Exception e)
                        {
                            var num = Double.Parse((expected[j] - outputs[j]).ToString().Substring(0, (expected[j] - outputs[j]).ToString().Length-5));
                            sum += float.Parse((Math.Pow(num, 2)).ToString().Substring(0, (Math.Pow(num, 2)).ToString().Length-5));
                        }
                    }
                    sumError += sum;
                    backwardPropagateError(expected);
                    updateWeights(row, lRate);
                }                
            }

            Debug.Log(">epoch=" + eCount + ", lrate=" + lRate +", error=" + sumError);
            eCount++;
        }
    }

    //Checklist [VVV]
    public float predict(List<float> row)
    {
        var outputs = forwardPropagate(row);

        var hasil = 0f;
        var simpan = 0f;
        
        var index = 0f;

        foreach (var o in outputs)
        {
            if (o > simpan)
            {
                simpan = o;
                hasil = index;
            }
            index++;
        }

        return hasil;
    }

    public void load()
    {
        initializeNetwork(PlayerPrefs.GetInt("nInputs"), PlayerPrefs.GetInt("nHiddens"), PlayerPrefs.GetInt("nOutputs"));
        
        var layerKe = 1;
        Debug.Log("LOADING JST...");
        foreach (var layer in network)
        {
            var neuronKe = 1;
            foreach (var neuron in layer)
            {
                var key = "[ Layer :" + layerKe.ToString()
                    + ", Neuron :" + neuronKe.ToString() + "";
                var weightKe = 1;
                for (var i=0;i<neuron.weights.Count;i++)
                {
                    var nilaiWeight = PlayerPrefs.GetFloat(key + ", Weight : " + weightKe.ToString() + " ]");
                    //Debug.Log(key + ", Weight : " + weightKe.ToString() + " ] =" + nilaiWeight);
                    neuron.weights[i] = nilaiWeight;
                    weightKe++;
                }
                neuronKe++;
            }
            layerKe++;
        }

        Debug.Log("LOADED");
    }

    public void train(List<List<float>> dataTraining, Boolean loadJST)
    {
        List<List<float>> dataset = dataTraining;
        List<List<float>> datasetBackup = new List<List<float>>();        

        foreach (var data in dataset)
        {
            datasetBackup.Add(data);
        }

        var nOutputs = 16;
        var nInputs = dataset[0].Count - 1;

        if (loadJST)
        {
            load();
        }
        else
        {
                       
            initializeNetwork(nInputs, nInputs * 2, nOutputs);          
        }

        PlayerPrefs.SetInt("nInputs", nInputs);
        PlayerPrefs.SetInt("nHiddens", nInputs * 2);
        PlayerPrefs.SetInt("nOutputs", nOutputs);

        trainNetwork(dataset, 0.5f, 1000, nOutputs);

        //Save JST
        var layerKe = 1;
        Debug.Log("SAVING JST...");
        foreach (var layer in network)
        {
            var neuronKe = 1;
            foreach (var neuron in layer)
            {
                var key = "[ Layer :" + layerKe.ToString()
                    + ", Neuron :" + neuronKe.ToString() + "";
                var weightKe = 1;
                foreach (var weight in neuron.weights)
                {
                    //Debug.Log(key + ", Weight : " + weightKe.ToString() + " ] ="+weight);
                    PlayerPrefs.SetFloat(key + ", Weight : " + weightKe.ToString() + " ]", weight);
                    weightKe++;
                }
                neuronKe++;
            }
            layerKe++;
        }
        PlayerPrefs.Save();
        Debug.Log("SAVED");

        foreach (var row in datasetBackup)
        {
            var prediction = predict(row);
            Debug.Log("Expected=" + row[row.Count - 1] + ", Got=" + prediction);
        }


    }

}

public class Neuron
{
    public List<float> weights;
    public float output;
    public float delta;

    public Neuron()
    {
        weights = new List<float>();
        output = 0f;
        delta = 0f;

    }
}