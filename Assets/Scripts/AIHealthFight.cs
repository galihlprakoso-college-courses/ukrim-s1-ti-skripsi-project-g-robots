﻿using UnityEngine;
using UnityEngine.UI;

public class AIHealthFight : MonoBehaviour {

    public float fullHealth;

    float currentHealth;

    AIController aiController;

    public Slider healtSlider;    

    // Use this for initialization
    void Start()
    {
        currentHealth = fullHealth;
        aiController = GetComponent<AIController>();

        healtSlider.maxValue = fullHealth;
        healtSlider.value = currentHealth;
        
    }

    public void addDamage(float damage)
    {
        if (damage <= 0) return;
        currentHealth -= damage;

        healtSlider.value = currentHealth;        

        if (currentHealth <= 0)
        {            
           backToMenu();
        }
    }



    public void backToMenu()
    {                
        Application.LoadLevel(0);
    }
}



