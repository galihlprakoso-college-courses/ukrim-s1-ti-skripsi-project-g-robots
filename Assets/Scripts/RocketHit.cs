﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketHit : MonoBehaviour {

    public float weaponDamage;
    public float pushBackForce;

    projectTileController myPC;

    public GameObject explosionEffect;

	// Use this for initialization
	void Awake () {
        myPC = GetComponentInParent<projectTileController>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);


            if (PlayerPrefs.GetInt("currentScene") == 1){
                AIHealth aH = collision.gameObject.GetComponent<AIHealth>();
                aH.addDamage(weaponDamage);
            }else{                
                AIHealthFight aH = collision.gameObject.GetComponent<AIHealthFight>();
                aH.addDamage(weaponDamage);
            }
                         
            pushBack(collision.transform);
        }        
    }

    private void pushBack(Transform pushedObject)
    {
        Vector2 pushDirection = new Vector2((pushedObject.position.x - transform.position.x),0).normalized;

        pushDirection *= pushBackForce;
        Rigidbody2D pushRB = pushedObject.gameObject.GetComponent<Rigidbody2D>();
        pushRB.velocity = Vector2.zero;
        pushRB.AddForce(pushDirection, ForceMode2D.Impulse);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);

            AIHealth aH = collision.gameObject.GetComponent<AIHealth>();
            aH.addDamage(weaponDamage);
            pushBack(collision.transform);
        }
    }
}
