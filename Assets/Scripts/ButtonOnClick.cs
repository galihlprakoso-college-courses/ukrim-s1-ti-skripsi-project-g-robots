﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonOnClick : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
        PlayerPrefs.Save();
	}	

    public void goToScene(int address)
    {
        PlayerPrefs.SetInt("currentScene", address);
        Debug.Log("address : " + address);
        Debug.Log("Status Training : " + PlayerPrefs.GetInt("Status Training"));
        if (address == 2 && PlayerPrefs.GetInt("Status Training")==1) {
            Application.LoadLevel(address);
        }
        else if(address == 1)
        {
            PlayerPrefs.SetInt("Status Training", 1);
            PlayerPrefs.Save();
            Application.LoadLevel(address);
        }
        else
        {
            Debug.Log("JST BELUM DI TRAINING");
        }
    }
}
