﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketHitPlayer : MonoBehaviour {

    public float weaponDamage;
    public float pushBackForce;

    projectTileController myPC;

    public GameObject explosionEffect;

    // Use this for initialization
    void Awake()
    {
        myPC = GetComponentInParent<projectTileController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAI")
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);

            if (PlayerPrefs.GetInt("currentScene") == 1)
            {
                PlayerHealth pH = collision.gameObject.GetComponent<PlayerHealth>();
                pH.addDamage(weaponDamage);
            }
            else
            {
                PlayerHealthFight pH = collision.gameObject.GetComponent<PlayerHealthFight>();
                pH.addDamage(weaponDamage);
            }
            
            pushBack(collision.transform);
        }
    }

    private void pushBack(Transform pushedObject)
    {
        Vector2 pushDirection = new Vector2((pushedObject.position.x - transform.position.x), 0).normalized;

        pushDirection *= pushBackForce;
        Rigidbody2D pushRB = pushedObject.gameObject.GetComponent<Rigidbody2D>();
        pushRB.velocity = Vector2.zero;
        pushRB.AddForce(pushDirection, ForceMode2D.Impulse);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "PlayerAI")
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);


            if (collision.tag == "PlayerAI")
            {
                myPC.removeForce();
                Instantiate(explosionEffect, transform.position, transform.rotation);
                Destroy(gameObject);


                PlayerHealth pH = collision.gameObject.GetComponent<PlayerHealth>();
                pH.addDamage(weaponDamage);
                pushBack(collision.transform);
            }
        }
    }
}
