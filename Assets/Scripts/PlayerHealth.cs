﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public float fullHealth;

    float currentHealth;

    PlayerController playerController;

    public GameObject aiObject;

    public Slider healthSlider;

    private JaringanSarafTiruan jst;

    public Text loadingText;

    // Use this for initialization
    void Start () {
        currentHealth = fullHealth;
        playerController = GetComponent<PlayerController>();

        healthSlider.maxValue = fullHealth;
        healthSlider.value = currentHealth;
        
        jst = new JaringanSarafTiruan();
	}

    public void addDamage(float damage)
    {
        if (damage <= 0) return;
        currentHealth -= damage;

        healthSlider.value = currentHealth;

        if (currentHealth <= 0)
        {
            loadingText.gameObject.SetActive(true);
            trainJST();
        }
    }

    public void trainJST()
    {
        //Play dead animation
        AIController aC = aiObject.GetComponent<AIController>();        

        foreach(var datas in aC.dataTraining)
        {
            var str = "[";
            foreach(var data in datas)
            {
                str += data.ToString() + ",";
            }
            str += "]";
            Debug.Log(str);
        }

        if (PlayerPrefs.GetInt("nInputs") == null)
        {
            jst.train(aC.dataTraining, false);
        }
        else
        {
            jst.train(aC.dataTraining, true);
        }

        Application.LoadLevel(0);
    }
}
