﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControllerFight : MonoBehaviour
{
    //Movement Variables
    public float maxSpeed;

    Rigidbody2D myRB;
    Animator myAnim;
    bool facingRight;
    bool tembak = false;

    //Jumping Variable
    bool grounded = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

    //For Shooting
    public Transform gunTip;
    public GameObject bullet;
    float fireRate = 0.5f;
    float nextFire = 0;

    //Musuh
    public GameObject musuh;


    //Lompat
    int durasiLompat = 0;
       

    private int waktuAddDataTraining;

    JaringanSarafTiruan jst;

    // Use this for initialization
    void Start()
    {
        maxSpeed = 40;
        jumpHeight = 10;
        myRB = GetComponent<Rigidbody2D>();
        myAnim = GetComponent<Animator>();
        facingRight = true;
        jumpHeight = 1500;

        waktuAddDataTraining = 0;

        jst = new JaringanSarafTiruan();

        jst.load();        

        flip();
        
        //jst.execute();        
    }

    void Update()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {        

        //check if we are grounded if no, then we are fall
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        myAnim.SetBool("isGrounded", grounded);

        myAnim.SetFloat("verticalSpeed", myRB.velocity.y);


        mikir();
    }

    void mikir()
    {
        List<float> data = new List<float>();
        //Inputs
        
        //Facing 
        if (facingRight == true)
        {
            data.Add(1f);
        }
        else
        {
            data.Add(0f);
        }

        //Position
        Vector3 theScale = transform.localPosition;
        data.Add(normalisasiInput(theScale.x));
        data.Add(normalisasiInput(theScale.y));

        //Posisi Musuh
        Vector3 musuhScale = musuh.transform.localPosition;
        data.Add(normalisasiInput(musuhScale.x));
        data.Add(normalisasiInput(musuhScale.y));

        //Posisi Peluru Musuh
        GameObject peluruMusuh = null;
        peluruMusuh = getClosestObject("peluru");
        
        if (peluruMusuh != null)
        {
            Vector3 posisiPeluru = peluruMusuh.transform.localPosition;
            data.Add(normalisasiInput(posisiPeluru.x));
            data.Add(normalisasiInput(posisiPeluru.y));
        }
        else
        {
            data.Add(0f);
            data.Add(0f);
        }

        //data.Add(0f);
        //data.Add(0f);

        data.Add(0f);

        aksi(jst.predict(data));
    }

    float normalisasiInput(float input)
    {
        //Debug.Log("INPUT : " + input);
        string strPembagi = "1";

        for (var i = 0; i < input.ToString().Length; i++)
        {
            strPembagi += "0";
        }

        string hasil = (input / float.Parse(strPembagi)).ToString().Substring(0, 4);

        return float.Parse(hasil) / 10f;
    }

    void flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void fireRocket()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (facingRight)
            {
                Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            }
            else if (!facingRight)
            {
                Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(-180f, 0, -180f)));
            }

        }
    }    

    float convertToDecimal(int[] outputArray)
    {
        Debug.Log("Masuk converToDecimal()");
        var stringBiner = "";        

        foreach(var o in outputArray)
        {
            stringBiner += o.ToString();
        }

        Debug.Log("Target Output : " + stringBiner);

        switch (stringBiner) {
            case "0000":
                return 0f;
            case "0001":
                return 1f;                
            case "0010":
                return 2f;                
            case "0011":
                return 3f;                
            case "0100":
                return 4f;                
            case "0101":
                return 5f;                
            case "0110":
                return 6f;                
            case "0111":
                return 7f;
            case "1000":
                return 8f;
            case "1001":
                return 9f;
            case "1010":
                return 10f;
            case "1011":
                return 11f;
            case "1100":
                return 12f;
            case "1101":
                return 13f;
            case "1110":
                return 14f;
            case "1111":
                return 15f;
            default:
                return 0f;
        }
        
    }

    private void aksi(float desimal)
    {
        Debug.Log("aksi : " + desimal);
        if (desimal == 0f)
        {
            Jalan("kiri");
        }else if(desimal == 1f)
        {
            Debug.Log("DIAM");
        }else if(desimal == 2f)
        {
            Jalan("kanan");
        }else if (desimal == 3f)
        {
            Debug.Log("DIAM");
        }else if(desimal == 4f)
        {
            Tembak();
            Jalan("kiri");
        }else if(desimal == 5f)
        {
            Tembak();
        }else if(desimal == 6f)
        {
            Tembak();
            Jalan("kanan");
        }else if(desimal == 7f)
        {
            Tembak();
        }else if(desimal == 8f)
        {
            Lompat();
            Jalan("kiri");
        }else if(desimal == 9f)
        {
            Lompat();
        }else if(desimal == 10f)
        {
            Lompat();
            Jalan("kanan");
        }else if(desimal == 11f)
        {
            Lompat();
        }else if(desimal == 12f)
        {
            Lompat();
            Tembak();
            Jalan("kiri");
        }else if(desimal == 13f)
        {
            Lompat();
            Tembak();
        }else if(desimal == 14f)
        {
            Lompat();
            Tembak();
            Jalan("kanan");
        }else if(desimal == 15f)
        {
            Lompat();
            Tembak();
        }
    }

    private void Lompat()
    {
        if (grounded)
        {
            grounded = false;
            myAnim.SetBool("isGrounded", grounded);
            myRB.AddForce(new Vector2(0, 3000));
        }        
        
    }

    private void Tembak()
    {
        fireRocket();
    }

    private void Jalan(string arah)
    {
        float move = 0f;

        if (arah == "kanan")
        {
            move = 0.5f;
        }
        else
        {
            move = -0.5f;
        }
        myAnim.SetFloat("speed", Mathf.Abs(move));

        myRB.velocity = new Vector2(move * maxSpeed, myRB.velocity.y);

        if (move > 0 && facingRight)
        {
            flip();
        }
        else if (move < 0 && !facingRight)
        {
            flip();
        }
    }

    GameObject getClosestObject(string tag)
    {
        var objectsWithTag = GameObject.FindGameObjectsWithTag(tag);
        GameObject closestObject = null;

        foreach(var obj in objectsWithTag)
        {
            if (closestObject==null)
            {
                closestObject = obj;
            }

            if(Vector3.Distance(transform.position,obj.transform.position)<= Vector3.Distance(transform.position, closestObject.transform.position))
            {
                closestObject = obj;
            }
        }
        return closestObject;
    }
}